appCliente.controller("clienteController", function($scope, $http){ 
	
	$scope.clientes = [];
	$scope.cliente = {};
	$scope.classesRisco = {
	    model: null,
	    options: [
	         {value: 'A', name: 'A'},
	         {value: 'B', name: 'B'},
	         {value: 'C', name: 'C'}
	    ]
	};
	
	$scope.cadastrarCliente = function() {
		
		$scope.msgErro = null;
		
		var req = {
			method: 'POST',
			url: 'http://localhost:8081/v1/cliente',
			headers: {
			  'Content-Type': 'application/json'
			},
			data: $scope.cliente
		}		

		$http(req).then(function(response){
			$scope.clientes.push(response.data);			
		}, function(response){
			var error = "Erro ao tentar cadastrar o cliente: " + $scope.cliente.nomeCliente;
			$scope.msgErro = error;
		});

	}	
	
});