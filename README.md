# riscocliente

Sistema de cadastro de cliente com calculo de juros referente a classificação de risco.

```mermaid
sequenceDiagram
      Usuário (Site)->>+API Cliente: Cadastra cliente
      API Cliente->>+API Risco: Calcula Risco
      API Risco-->>API Cliente: Retorna Juros
      API Cliente->>+Banco de Dados: Grava Cliente
      Banco de Dados-->>-API Cliente: Sucesso
      API Cliente-->>-Usuário (Site): Sucesso (204)
```

## Setup

Iniciar todos os projetos com springboot e entrar no endereço http://localhost:8080/
