package br.com.azevedo.apirisco;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiriscoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiriscoApplication.class, args);
	}
}
