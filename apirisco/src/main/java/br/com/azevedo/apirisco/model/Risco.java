package br.com.azevedo.apirisco.model;

import java.math.BigDecimal;

public class Risco {

	private char classeRisco;
	private BigDecimal taxaJuros;
	
	public char getClasseRisco() {
		return classeRisco;
	}
	public void setClasseRisco(char classeRisco) {
		this.classeRisco = classeRisco;
	}
	public BigDecimal getTaxaJuros() {
		return taxaJuros;
	}
	public void setTaxaJuros(BigDecimal taxaJuros) {
		this.taxaJuros = taxaJuros;
	}
	
}
