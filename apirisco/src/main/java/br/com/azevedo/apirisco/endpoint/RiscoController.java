package br.com.azevedo.apirisco.endpoint;

import java.math.BigDecimal;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.azevedo.apirisco.model.Risco;

@RestController
@RequestMapping("/v1")
public class RiscoController {

	/**
	 * @author Edson Azevedo
	 * @description Calculo de taxa de juros de acordo com classificação de risco do cliente
	 * @since 2018-12-05
	 * @param risco
	 * @return riscoCalculado
	 */
	@RequestMapping(method=RequestMethod.GET, value="/risco/juros/{classeRisco}", produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Risco> calculaJuros(@PathVariable String classeRisco) {
		
		Risco risco = new Risco();
		risco.setClasseRisco(classeRisco.charAt(0));
		
		// Aplica regra de Juros por Risco
		switch (String.valueOf(risco.getClasseRisco()).toUpperCase()) {
			case "B":
				risco.setTaxaJuros(new BigDecimal(10));
				break;
			case "C":
				risco.setTaxaJuros(new BigDecimal(20));
				break;			
		}
		
		return new ResponseEntity<Risco>(risco, HttpStatus.CREATED);
	}
		
}