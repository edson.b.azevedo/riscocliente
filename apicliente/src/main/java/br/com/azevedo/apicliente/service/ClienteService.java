package br.com.azevedo.apicliente.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.azevedo.apicliente.model.Cliente;
import br.com.azevedo.apicliente.repository.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	ClienteRepository clienteRepository;
	
	public Cliente cadastrarCliente(Cliente calculoJurosModel) {
		
		return clienteRepository.save(calculoJurosModel);
		
	}

	
}