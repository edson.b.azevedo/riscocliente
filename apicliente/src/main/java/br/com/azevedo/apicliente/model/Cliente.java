package br.com.azevedo.apicliente.model;

import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Cliente {
	
	@Id
	@GeneratedValue
	private Integer id;
	private String nomeCliente;
	private BigDecimal valorLimiteCredito;
	private char classeRisco;
	private BigDecimal taxaJuros;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public BigDecimal getValorLimiteCredito() {
		return valorLimiteCredito;
	}
	public void setValorLimiteCredito(BigDecimal valorLimiteCredito) {
		this.valorLimiteCredito = valorLimiteCredito;
	}
	public char getClasseRisco() {
		return classeRisco;
	}
	public void setClasseRisco(char classeRisco) {
		this.classeRisco = classeRisco;
	}
	public BigDecimal getTaxaJuros() {
		return taxaJuros;
	}
	public void setTaxaJuros(BigDecimal taxaJuros) {
		this.taxaJuros = taxaJuros;
	}

}
