package br.com.azevedo.apicliente.endpoint;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import br.com.azevedo.apicliente.model.Cliente;
import br.com.azevedo.apicliente.model.Risco;
import br.com.azevedo.apicliente.service.ClienteService;

@RestController
@RequestMapping("/v1")
public class ClienteController {

	@Autowired
	ClienteService clienteService;
	
	private RestTemplate rest = new RestTemplate();
		
	/**
	 * @author Edson Azevedo
	 * @description Cadastra cliente com calculo de taxa de juros de acordo com classificação de risco
	 * @since 2018-12-05
	 * @param cliente
	 * @return clienteCadastrado
	 */
	@CrossOrigin(origins = "http://localhost:8080")
	@RequestMapping(method=RequestMethod.POST, value="/cliente", consumes=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Cliente> calculaJuros(@RequestBody Cliente cliente) {
				
		// Aplica regra de Juros por Risco
		BigDecimal jurosCalculado = calculaJuros(cliente.getClasseRisco()).getTaxaJuros();
		cliente.setTaxaJuros(jurosCalculado);
		
		// Chama serviço para cadastrar cliente
		Cliente clienteCadastrado = clienteService.cadastrarCliente(cliente);
		
		return new ResponseEntity<Cliente>(clienteCadastrado, HttpStatus.CREATED);
	}

	
    private Risco calculaJuros(char classeRisco) {
    	
    	Map<String,String> uriVars = new HashMap<String,String>();
    	uriVars.put("classeRisco", String.valueOf(classeRisco));
    	
        String uri = "http://localhost:8082/v1/risco/juros/" + classeRisco;
        Risco risco = rest.getForObject(uri, Risco.class);

        return risco;
    }

}