package br.com.azevedo.apicliente.repository;

import org.springframework.stereotype.Repository;
import br.com.azevedo.apicliente.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface ClienteRepository extends JpaRepository<Cliente,Integer> {
	
	

}
